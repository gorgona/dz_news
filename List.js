/**
 * Created by Elena on 25.07.2016.
 */
function List(id, timeout, collor) {
    var items = [];
    var itemsCentre = [];
    var isStopped = false;
    var el = document.querySelector(id);
    var elCentre = document.querySelector('#center');

    el.classList.add('list-collor-' + collor);

    el.addEventListener('click', clickHandler);


    this.stop = function() {
        isStopped = true;
    };

    this.start = function() {
        isStopped = false;
    };

    // �������� ��������
    function removeItem(domElement) {
        var index = items.indexOf(domElement.innerHTML);
        console.log('innerHTML '+domElement.innerHTML);
        itemsCentre.unshift(domElement.innerHTML);
        console.log('Length '+itemsCentre.length);
        items.splice(index, 1);

    }

    function createItem(text) {
        var item = document.createElement('div');
        item.classList.add('item');
        item.innerHTML = text;
        return item;
    }


    function clickHandler (event) {
        console.dir(event.target);
        if(event.target.classList.contains('item')) {
            removeItem(event.target);
            generateText(render);
            renderCentre();
        }
    }


    function addItem(text) {
        // 1 array push
        if (!onClickStopButton) {
            items.unshift(text);
        }
        render();
    }

    function render() {
        var container = document.createDocumentFragment();

        items.forEach(function(item) {
            container.appendChild(createItem(item));
        });

        // 3 clear el container
        el.innerHTML = '';
        // 4 add list to main container el
        el.appendChild(container);
    }

    function renderCentre() {
        var container = document.createDocumentFragment();

        itemsCentre.forEach(function(item) {
            container.appendChild(createItem(item));
        });

        // 3 clear el container
        elCentre.innerHTML = '';
        // 4 add list to main container el
        elCentre.appendChild(container);
    }

    function generateText(fn) {
        if (isStopped) {
            return;
        }

        // 1 generate
        var newText = ItemsText.more();
        fn(newText);
        // 5 setTimeout

        setTimeout(function() {
            generateText(fn);
        }, timeout);
    }



    generateText(addItem);
}

