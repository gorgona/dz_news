/**
 * Created by Elena on 20.07.2016.
 */
function Timer( options ) {
    //public
    var defaultOptions = {
        delay: 20 //����� ����� �������� ������� � ��, ������ ����� - ����� ����
        ,stopFrame: 0 // �������� ����
        ,loop: true // ���� ����������� ��������
        ,frameElementPrefixId: 'timer_' // ������� ���������� ID ���������, ��� ���������-������������ ��������

    }
    // ���� ����� �� �������, ��� ����� ������� �� ���������, ���� ������� - ���������
    for(var option in defaultOptions) this[option] = options && options[option]!==undefined ? options[option] : defaultOptions[option];

    //private
    var busy = false, // ���� "�����" ��� "������� ����!"
        currentTime = 0 // ������� �����
        ,frame = 1	// ������� ����
        ,task = {}	// �! �����! � ���� ������ ����� �������� ������ ����� ��� ����������, �� ������
        ,keyFrames=[]	// ������ �������� �������� ������, �.�. ��� � ������� �������� ������
        ;

    this.getKeyFrames = function( ) {
        return keyFrames;
    }
};

var timer = new Timer( {delay: 500, loop: false } );