/**
 * Created by Elena on 20.07.2016.
 */



function List(id, timeout, collor) {
    var items = [];
    var isStopped = false;
    var el = document.querySelector(id);
    el.classList.add('list-collor-' + collor);

    el.addEventListener('click', clickHandler);


    this.stop = function() {
        isStopped = true;
    };

    this.start = function() {
        isStopped = false;
    };

    // �������� ��������
    function removeItem(domElement) {
        var index = items.indexOf(domElement.innerHTML);
        items.splice(index, 1);
    }

    function clickHandler (event) {
        console.dir(event.target);
        if(event.target.classList.contains('item')) {
            removeItem(event.target);
        }
    }

    function createItem(text) {
        var item = document.createElement('div');
        item.classList.add('item');
        item.innerHTML = text;
        return item;
    }

    function render(text) {
        // 1 array push
        items.unshift(text);

        // 2 make html list
        var container = document.createDocumentFragment();

        items.forEach(function(item) {
            container.appendChild(createItem(item));
        });

        // 3 clear el container
        el.innerHTML = '';
        // 4 add list to main container el
        el.appendChild(container);
    }

    function generateText(fn) {
        if (isStopped) {
            return;
        }

        // 1 generate
        var newText = ItemsText.more();
        fn(newText);
        // 5 setTimeout

        setTimeout(function() {
            generateText(fn);
        }, timeout);
    }

    generateText(render);
}



var leftList = new List('#left', 10000, 'red');
var rightList = new List('#right', 10000, 'blue');
// var centerList = new List('#center', 4000);

